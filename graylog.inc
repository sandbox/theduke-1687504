<?php

/**
 * @file
 * Helper functions for Graylog.
 *
 */

/**
 * Load the GELF-PHP library components.
 *
 */
function _graylog_load_gelf($force=false) {
  static $status;
  if (is_bool($status) && !$force) return $status;

  $path = 'sites' . DIRECTORY_SEPARATOR . 'all' . DIRECTORY_SEPARATOR
  . 'libraries' . DIRECTORY_SEPARATOR . 'Graylog2-gelf-php' . DIRECTORY_SEPARATOR;

  include_once $path . 'GELFMessage.php';
  include_once $path . 'GELFMessagePublisher.php';

  $status = class_exists('GELFMessage') && class_exists('GELFMessagePublisher');

  if (!$status) {
    watchdog(
        'graylog',
        'Could not load GELF-PHP library. Files not present?',
        array(),
        WATCHDOG_CRITICAL);
  }
  
  return $status;
}

/**
 * Get graylog settings and optionally validate them.
 * 
 * Returns false if no setttings available or invalid.
 * 
 * @param boolean $validate
 * @return array
 */
function _graylog_get_settings($validate=true) {
  $data = variable_get(GRAYLOG_VAR_SETTINGS);
  
  if (!$validate) return $data;
  
  $valid = true;
  
  $required = array(
    'enabled',
    'project',
    'environment',
    'graylog_server',
    'graylog_port',
  );
  
  if (!is_array($data)) $valid = false;  
  foreach ($required as $key) if (!array_key_exists($key, $data)) $valid = false; 
  if (!is_numeric($data['graylog_port'])) $valid = false;
  
  return $valid ? $data : false;
}

/**
 * Take a hook_watchdog like $log_entry and build a GELF-PHP message object.
 * 
 * @param array $log_entry
 * @param array $settings
 * @return GELFMessage
 */
function _graylog_build_message(array $log_entry, array $settings) {
  // build the graylog message object
  $message = new GELFMessage();
  
  $vars = is_array($log_entry['variables']) ? $log_entry['variables'] : array();
  $text = t($log_entry['message'], $vars);
  
  $message->setShortMessage($text);
  //$message->setFullMessage('');
  $message->setHost($settings['project']);
  $message->setLevel((int) $log_entry['severity']);
  $message->setFacility($log_entry['type']);
  $message->setTimestamp($log_entry['timestamp']);
  
  $message->setAdditional("environment", $settings['environment']);
  $message->setAdditional("link", $log_entry['link']);
  $message->setAdditional("location", $log_entry['request_uri']);
  $message->setAdditional("referer", $log_entry['referer']);
  $message->setAdditional("hostname", $log_entry['ip']);
  $message->setAdditional("user_id", (int) $log_entry['uid']);
  
  return $message;
}

/**
 * Get GELF-PHP message publisher.
 * 
 * @param array $settings
 * @return GELFMessagePublisher
 */
function _graylog_get_publisher(array $settings) {
  static $publisher;
  
  if (!$publisher) {
    // create GELF-PHP publisher
    $publisher = new GELFMessagePublisher(
      $settings['graylog_server'], 
      $settings['graylog_port']);
  }
  
  return $publisher;
}
