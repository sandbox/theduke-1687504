<?php
/**
 * @file
 * Drush commands for pushing log entries to graylog.
 * 
 */

/**
 * Implementation of hook_drush_command().
 *
 *  Supplies drush command for downloading the GELF-PHP library.
 */
function graylog_drush_command() {
  $items = array();

  $items['graylog-download-gelf'] = array(
    'description' => 'Download the GELF-PHP library.',
    'bootstrap' => DRUSH_BOOTSTRAP_DRUPAL_FULL,
    'options' => array(),
  );

  return $items;
}

/**
 * Implementation of the drush command graylog-download-gelf
 */
function drush_graylog_download_gelf() {
  $lib_path = drush_get_context('DRUSH_DRUPAL_ROOT') . DIRECTORY_SEPARATOR . 'sites' . DIRECTORY_SEPARATOR . 'all' . DIRECTORY_SEPARATOR . 'libraries';
  if (!is_dir($lib_path) && !mkdir($lib_path)) {
    drush_log('Could not create libraries folder ' . $lib_path, 'error');
    return;
  }

  $url = 'https://github.com/Graylog2/gelf-php/zipball/master';
  $local_path = $lib_path . DIRECTORY_SEPARATOR . 'gelf.zip';

  drush_log('Downloading GELF-PHP from ' . $url . '...', 'info');

  if (!copy($url, $local_path)) {
    drush_log('Could not download library from ' . $url, 'error');
    return;
  }

  exec(sprintf('unzip "%s" -d "%s"', $local_path, $lib_path));

  $dirname = NULL;

  $handle = opendir($lib_path);
  while (($file = readdir($handle)) !== FALSE) {
    if (strpos($file, 'Graylog2-gelf') !== FALSE) {
    $dirname = $lib_path . DIRECTORY_SEPARATOR . $file;
      break;
    }
  }

  if (!$dirname) {
    drush_log('Could not find unzipped folder.', 'error');
    return;
  }

  rename($dirname, $lib_path . DIRECTORY_SEPARATOR . 'Graylog2-gelf-php');
  unlink($local_path);

  drush_log('Successfully downloaded GELF-PHP into sites/all/libraries', 'success');
}
