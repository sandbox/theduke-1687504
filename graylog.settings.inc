<?php
/**
 * @file
 * Settings for Graylog.
 *
 */

/**
 * Settings form
 *
 * @param array $form_state
 * @return array 
 *   the form
 */
function graylog_settings_form($form, &$form_state) {
	watchdog('testmodule', 'This is a test', array(), WATCHDOG_CRITICAL);
	
  $settings = variable_get(GRAYLOG_VAR_SETTINGS, array());

  $form['enabled'] = array(
    '#type'  => 'checkbox',
    '#title' => t('Enable Pushing'),
    '#default_value' => isset($settings['enabled']) ? $settings['enabled'] : '',
  );
 
  $form['project'] = array(
    '#type'  => 'textfield',
    '#required'  => TRUE,
    '#title' => t('Project'),
    '#description' => t('Name for the project that will be used to categorize this sites log messages.'),
    '#default_value' => isset($settings['project']) ? $settings['project'] : '',
  );

  $form['environment'] = array(
    '#type'  => 'textfield',
    '#required'  => TRUE,
    '#title' => t('Environment'),
    '#description' => t('Current environment like "development", "production".'),
    '#default_value' => isset($settings['environment']) ? $settings['environment'] : '',
  );

  $form['graylog_server'] = array(
    '#type'  => 'textfield',
    '#required'  => TRUE,
    '#title' => t('Graylog2 Server'),
    '#description' => t('IP or host for server.'),
    '#default_value' => isset($settings['graylog_server']) ? $settings['graylog_server'] : '',
  );
  
  $form['graylog_port'] = array(
      '#type'  => 'textfield',
      '#required'  => TRUE,
      '#title' => t('Graylog2 Port'),
      '#description' => t('Server port. Default is 12201.'),
      '#default_value' => isset($settings['graylog_port']) ? $settings['graylog_port'] : '',
  );

  $form['submit'] = array(
    '#type'  => 'submit',
    '#value' => t('Submit'),
  );

  return $form;
}

/**
 * Validate the settings form.
 *
 */
function graylog_settings_form_validate($form, &$form_state) {
  $values = $form_state['values'];

  if (!($values['graylog_port'] && is_numeric($values['graylog_port']))) {
    form_set_error('graylog_port', t('Port must be numeric.'));
  }
}

/**
 * Submit the settings form
 *
 */
function graylog_settings_form_submit($form, &$form_state) {
  $values = $form_state['values'];
  
  variable_set(GRAYLOG_VAR_SETTINGS, $values);
  drupal_set_message(t('Settings have been updated.'));
}
